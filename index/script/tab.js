async function changeTab(tabIndex) {
  if (tabIndex === 1) {
    const gitImgList = document.getElementsByClassName("git-img");
    if (gitImgList.length === 0) getImg();
  }
  // 获取所有标签和内容区域元素
  const tabs = document.getElementsByClassName("tab");
  const contents = document.getElementsByClassName("tab-content");

  // 移除所有标签的活动样式类
  for (const tabItem of tabs) {
    tabItem.classList.remove("active-tab");
  }

  // 隐藏所有内容区域
  for (const content of contents) {
    content.style.display = "none";
  }

  // 添加活动样式类到选中的标签，并显示对应的内容区域
  tabs[tabIndex].classList.add("active-tab");
  contents[tabIndex].style.display = "block";
}
